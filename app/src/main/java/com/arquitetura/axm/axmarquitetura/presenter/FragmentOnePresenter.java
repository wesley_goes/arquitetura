package com.arquitetura.axm.axmarquitetura.presenter;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arquitetura.axm.axmarquitetura.R;
import com.arquitetura.axm.axmarquitetura.appcontroller.enums.FragmentsEnum;
import com.arquitetura.axm.axmarquitetura.appcontroller.utils.FragmentBuilder;
import com.arquitetura.axm.axmarquitetura.presenter.events.FragmentActionListener;
import com.arquitetura.axm.axmarquitetura.presenter.interfaces.IFragmentOnePresenter;
import com.arquitetura.axm.axmarquitetura.view.FragmentOneView;
import com.arquitetura.axm.axmarquitetura.view.interfaces.IFragmentOneView;


public class FragmentOnePresenter extends AbstractPresenter implements IFragmentOnePresenter {
	private FragmentActivity activity;
	private IFragmentOneView view;





	@Override
	public void onAttach(Activity activity) {
		if(activity instanceof FragmentActivity){
			this.activity = (FragmentActivity) activity;
		}
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle bundle) {
		final ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_one, container, false);
		createView(view);
		setRetainInstance(true);

		return view;
	}


	@Override
	public FragmentActivity getLocalActivity() {
		return activity;
	}


	@Override
	public void createView(ViewGroup view) {
		this.view =  new FragmentOneView(view);

		super.createView(view);
	}

	@Override
	public void saveData(String nome, int idade) {

	}



	@Override
	public void setUpListeners() {
		this.view.setButtonOnclickListener(new FragmentActionListener.ShowFragmentOneOnClickListener(this));
	}

	@Override
	public IFragmentOneView getLocalView() {
		return this.view;
	}

	@Override
	public void setText() {
		FragmentBuilder.addFragment(activity, android.R.id.content, FragmentsEnum.FRAGMENT_TWO);
	}



}