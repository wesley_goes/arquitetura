package com.arquitetura.axm.axmarquitetura.appcontroller;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.arquitetura.axm.axmarquitetura.appcontroller.enums.FragmentsEnum;
import com.arquitetura.axm.axmarquitetura.appcontroller.utils.FragmentBuilder;


public class AppController extends FragmentActivity {
	private static final String FRAGMENT_SIZE="fragment_size";
	@Override
	protected void onCreate(final Bundle bundle) {
		super.onCreate(bundle);
//		DisplayUtil.init(this);
//		Utils.initFonts(this);

		if (bundle == null) {
			FragmentBuilder.addFragment(this, android.R.id.content, FragmentsEnum.SPLASH_SCREAM);

		} else {
			final int size = bundle.getInt(FRAGMENT_SIZE);
			FragmentBuilder.getInstance().setSize(size);
		}
	}

	@Override
	public void onBackPressed() {
		if(FragmentBuilder.getInstance().getSize()<=1){
			FragmentBuilder.setInstance(null);
			super.onBackPressed();
		}else{
			FragmentBuilder.onBackPressed(this);
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putInt(FRAGMENT_SIZE, FragmentBuilder.getInstance().getSize());
		super.onSaveInstanceState(outState);
	}
	
}