package com.arquitetura.axm.axmarquitetura.model.persistence;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.arquitetura.axm.axmarquitetura.model.domain.FuncionarioAxm;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

/**
 * Created by Wesley Goes on 14/08/2015.
 */
public class AxmDataBaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "axm.sqlite";
    private static final int DATABASE_VERSION = 1;

    public AxmDataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, FuncionarioAxm.class);

        } catch (java.sql.SQLException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource,
                          int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, FuncionarioAxm.class, true);
            onCreate(db, connectionSource);
        } catch (java.sql.SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        super.close();
    }



}
