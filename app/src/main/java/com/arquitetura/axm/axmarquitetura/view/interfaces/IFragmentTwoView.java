package com.arquitetura.axm.axmarquitetura.view.interfaces;


import android.view.View.OnClickListener;
import android.widget.BaseAdapter;

/**
 * Created by Wesley Goes on 13/08/2015.
 */
public interface IFragmentTwoView {
    void setFragmentRwoAdapter(BaseAdapter adapter);
    void setText(String text);
    void setBtBackOnclickListener(OnClickListener listener);
}
