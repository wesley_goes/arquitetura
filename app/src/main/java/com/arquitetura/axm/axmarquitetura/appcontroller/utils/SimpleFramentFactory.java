package com.arquitetura.axm.axmarquitetura.appcontroller.utils;
import android.support.v4.app.Fragment;

import com.arquitetura.axm.axmarquitetura.appcontroller.enums.FragmentsEnum;

public class SimpleFramentFactory {
	private static SimpleFramentFactory instance = new SimpleFramentFactory();

	public static SimpleFramentFactory getInstance() {
		return instance;
	}

	public Fragment create(final FragmentsEnum fragmentId) {
		try {
			return fragmentId.getClassFrag().newInstance();
		} catch (InstantiationException e) {
			throw new IllegalArgumentException(e.getMessage());
		} catch (IllegalAccessException e) {
			throw new IllegalArgumentException(e.getMessage());
		}
	}
}
