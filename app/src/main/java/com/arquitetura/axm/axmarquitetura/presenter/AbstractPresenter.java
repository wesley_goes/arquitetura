package com.arquitetura.axm.axmarquitetura.presenter;

import android.support.v4.app.Fragment;
import android.view.ViewGroup;

public abstract class AbstractPresenter extends Fragment {
	
	public void createView(final ViewGroup view){
		this.setUpListeners();
	}
	
	public abstract void setUpListeners();
}
