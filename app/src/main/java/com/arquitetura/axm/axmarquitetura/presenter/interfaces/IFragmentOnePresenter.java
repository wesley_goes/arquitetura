package com.arquitetura.axm.axmarquitetura.presenter.interfaces;

import android.support.v4.app.FragmentActivity;

import com.arquitetura.axm.axmarquitetura.view.interfaces.IFragmentOneView;

public interface IFragmentOnePresenter {
	FragmentActivity getLocalActivity();
	IFragmentOneView getLocalView();
	void setText();
	void saveData(String nome, int idade);

}  