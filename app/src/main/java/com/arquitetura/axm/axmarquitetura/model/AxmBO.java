package com.arquitetura.axm.axmarquitetura.model;

/**
 * Created by wesleygoes on 17/08/15.
 */
public class AxmBO {
    private static AxmBO instance;
    private static final Object SYNCOBJECT = new Object();

    public static AxmBO getInstance() {
        synchronized (SYNCOBJECT) {
            if (instance == null) {
                instance = new AxmBO();
            }
        }
        return instance;
    }



}
