package com.arquitetura.axm.axmarquitetura.view.interfaces;


import android.view.View.OnClickListener;

/**
 * Created by Wesley Goes on 13/08/2015.
 */
public interface IFragmentOneView {
    String getText();
    void setText(String text);
    void setButtonOnclickListener(OnClickListener listener);
}
