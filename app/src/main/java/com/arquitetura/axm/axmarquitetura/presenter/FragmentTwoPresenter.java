package com.arquitetura.axm.axmarquitetura.presenter;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arquitetura.axm.axmarquitetura.R;
import com.arquitetura.axm.axmarquitetura.appcontroller.enums.FragmentsEnum;
import com.arquitetura.axm.axmarquitetura.appcontroller.utils.FragmentBuilder;
import com.arquitetura.axm.axmarquitetura.presenter.events.FragmentActionListener;
import com.arquitetura.axm.axmarquitetura.presenter.interfaces.IFragmentTwoPresenter;
import com.arquitetura.axm.axmarquitetura.view.FragmentTwoView;
import com.arquitetura.axm.axmarquitetura.view.interfaces.IFragmentTwoView;


public class FragmentTwoPresenter extends AbstractPresenter implements IFragmentTwoPresenter {
	private FragmentActivity activity;
	private IFragmentTwoView view;
	@Override
	public void onAttach(Activity activity) {
		if(activity instanceof FragmentActivity){
			this.activity = (FragmentActivity) activity;
		}
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle bundle) {
		final ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_two, container, false);
		createView(view);
		setRetainInstance(true);
		return view;
	}


	@Override
	public FragmentActivity getLocalActivity() {

		return activity;
	}


	@Override
	public void createView(ViewGroup view) {
		this.view =  new FragmentTwoView(view);
		super.createView(view);
	}

	@Override
	public void setUpListeners() {
		this.view.setBtBackOnclickListener(new FragmentActionListener.ShowFragmentTwoOnClickListener(this));
	}

	@Override
	public IFragmentTwoView getLocalView() {
		return this.view;
	}

	@Override
	public void back() {
		FragmentBuilder.addFragment(activity, android.R.id.content, FragmentsEnum.FRAGMENT_ONE);
	}


}