package com.arquitetura.axm.axmarquitetura.appcontroller.enums;


import com.arquitetura.axm.axmarquitetura.presenter.AbstractPresenter;
import com.arquitetura.axm.axmarquitetura.presenter.FragmentOnePresenter;
import com.arquitetura.axm.axmarquitetura.presenter.FragmentTwoPresenter;
import com.arquitetura.axm.axmarquitetura.presenter.SplashPresenter;

public enum FragmentsEnum {
	SPLASH_SCREAM(1,SplashPresenter.class),
	FRAGMENT_ONE(2,FragmentOnePresenter.class),
	FRAGMENT_TWO(3,FragmentTwoPresenter.class);
	
	private int framentId;
	private Class<? extends AbstractPresenter> classFrag;
	/**
	 * Constructor
	 */
	private FragmentsEnum(final int framentId, final Class<? extends AbstractPresenter> classFrag) {
		this.framentId = framentId;
		this.classFrag = classFrag;
	}

	public int getFragmentId() {
		return framentId;
	}

	public Class<? extends AbstractPresenter> getClassFrag() {
		return classFrag;
	}
}
