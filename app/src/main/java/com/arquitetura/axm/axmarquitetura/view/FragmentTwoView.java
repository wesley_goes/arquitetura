package com.arquitetura.axm.axmarquitetura.view;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.arquitetura.axm.axmarquitetura.R;
import com.arquitetura.axm.axmarquitetura.view.interfaces.IFragmentTwoView;

/**
 * Created by Wesley Goes on 13/08/2015.
 */
public class FragmentTwoView implements IFragmentTwoView{

    private transient TextView txtTitle;
    private transient Button btBack;
    private transient ImageView imgBack;

    public FragmentTwoView(ViewGroup view){
        setImgBack(view);
        setBtBack(view);
    }

    private void setImgBack(ViewGroup view) {
        if(view.findViewById(R.id.img_back_fragone) instanceof  ImageView){
            this.imgBack = (ImageView)view.findViewById(R.id.img_back_fragone);
        }
    }

    private void setBtBack(ViewGroup view) {
        if(view.findViewById(R.id.bt_back) instanceof  Button){
            this.btBack = (Button)view.findViewById(R.id.bt_back);
        }
    }

    @Override
    public void setFragmentRwoAdapter(BaseAdapter adapter) {

    }

    @Override
    public void setText(String text) {

    }

    @Override
    public void setBtBackOnclickListener(View.OnClickListener listener) {
        if (this.imgBack != null){
            this.imgBack.setOnClickListener(listener);
        }
        if (this.btBack != null){
            this.btBack.setOnClickListener(listener);
        }
    }

}
