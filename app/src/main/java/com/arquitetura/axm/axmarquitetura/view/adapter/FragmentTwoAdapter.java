package com.arquitetura.axm.axmarquitetura.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.arquitetura.axm.axmarquitetura.R;
import com.arquitetura.axm.axmarquitetura.model.domain.FuncionarioAxm;
import com.arquitetura.axm.axmarquitetura.presenter.interfaces.IFragmentTwoPresenter;
import com.arquitetura.axm.axmarquitetura.view.FragmentTwoItemView;
import com.arquitetura.axm.axmarquitetura.view.interfaces.IFragmentTwoItemView;

import java.util.List;

/**
 * Created by Wesley Goes on 17/08/2015.
 */
public class FragmentTwoAdapter extends BaseAdapter {
    private IFragmentTwoPresenter presenter;
    private List<FuncionarioAxm> listFunciionario;
    public FragmentTwoAdapter(final IFragmentTwoPresenter presenter, List<FuncionarioAxm> listFunciionario){
        this.presenter = presenter;
        this.listFunciionario = listFunciionario;
    }
    @Override
    public int getCount() {
        return listFunciionario == null? 0 : listFunciionario.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return (long) position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewGroup view = null;
        view = (ViewGroup) LayoutInflater.from(parent.getContext()).inflate(R.layout.frag_two_item,null);

//        FuncionarioAxm funcionario = listFunciionario.get(position);
        IFragmentTwoItemView fragmentView =  new FragmentTwoItemView(view);

//        fragmentView.setName(funcionario.getName());
        fragmentView.setName(listFunciionario.get(position).getName());
        fragmentView.setAge(listFunciionario.get(position).getIdade());

        return view;
    }
}
