package com.arquitetura.axm.axmarquitetura.appcontroller.busines;

import android.content.Context;

import com.arquitetura.axm.axmarquitetura.model.domain.FuncionarioAxm;
import com.arquitetura.axm.axmarquitetura.model.persistence.AxmSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by wesleygoes on 17/08/15.
 */
public class AmbienteManager implements IMyAxmBusinessLogic {

    public static final String KEY = "MyPrayerBO";
    private static AmbienteManager SINGLETON;

    private AxmSqliteOpenHelper sqLite;

    public static AmbienteManager getInstance() {

        if (SINGLETON == null) {
            SINGLETON = new AmbienteManager();
        }

        return SINGLETON;
    }

    public void startSession(Context ctx) {
        sqLite = new AxmSqliteOpenHelper(ctx);
        sqLite.getWritableDatabase();
    }

    @Override
    public <T> T searchById(Class<T> clazz, Long id) throws SQLException {
        Dao<T, Long> dao = sqLite.getDao(clazz);
        return dao.queryForId(id);
    }

    @Override
    public <T> T searchByItem(String item, String local) throws SQLException {
        return null;
    }

    @Override
    public <T> T searchByItemNew(String item) throws SQLException {
        return null;
    }

    @Override
    public void insertAxmArquitetura(FuncionarioAxm funcionarioAxm) throws SQLException, ObjectAlreadyExistException {
        Dao<FuncionarioAxm, Long> dao = sqLite.getDao(FuncionarioAxm.class);
        List<FuncionarioAxm> result = dao.queryForEq("employee_name", funcionarioAxm.getName());
        if (result != null && result.size() > 0) {
            throw new ObjectAlreadyExistException();
        } else {
            sqLite.getDao(FuncionarioAxm.class).create(funcionarioAxm);
        }
    }

    @Override
    public void updateAxmArquitetura(FuncionarioAxm funcionarioAxm) throws SQLException {
        UpdateBuilder updateBuilder = sqLite.getDao(FuncionarioAxm.class).updateBuilder();
        updateBuilder.where().eq("auditing_id", funcionarioAxm.getId());
        updateBuilder.updateColumnValue("employee_name", funcionarioAxm.getName());
        updateBuilder.updateColumnValue("employee_age", funcionarioAxm.getAge());
        updateBuilder.update();
    }

    @Override
    public void deleteAxmArquitetura(FuncionarioAxm funcionarioAxm) throws SQLException {
        DeleteBuilder db = sqLite.getDao(FuncionarioAxm.class).deleteBuilder();
        db.where().eq("auditing_id", funcionarioAxm.getId());
        sqLite.getDao(FuncionarioAxm.class).delete(funcionarioAxm);
    }

    @Override
    public void deleteAll() throws SQLException {
        sqLite.getDao(FuncionarioAxm.class).delete(getEmployee());
    }

    @Override
    public List<FuncionarioAxm> getEmployee() throws SQLException {
        return sqLite.getDao(FuncionarioAxm.class).queryForAll();
    }

    @Override
    public List<FuncionarioAxm> getEmployeeReport(boolean state) throws SQLException {
        return null;
    }

    @Override
    public List<FuncionarioAxm> getEmployeeVerify() throws SQLException {
        return null;
    }
}
