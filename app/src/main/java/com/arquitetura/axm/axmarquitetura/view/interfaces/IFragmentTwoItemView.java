package com.arquitetura.axm.axmarquitetura.view.interfaces;


/**
 * Created by Wesley Goes on 13/08/2015.
 */
public interface IFragmentTwoItemView {
    void setName(String name);
    void setAge(int age);

}
