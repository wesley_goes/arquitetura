package com.arquitetura.axm.axmarquitetura.presenter;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arquitetura.axm.axmarquitetura.R;
import com.arquitetura.axm.axmarquitetura.appcontroller.enums.FragmentsEnum;
import com.arquitetura.axm.axmarquitetura.appcontroller.utils.FragmentBuilder;
import com.arquitetura.axm.axmarquitetura.presenter.interfaces.ISplashPresenter;


public class SplashPresenter extends AbstractPresenter implements ISplashPresenter {
	private static final long DELAY = 3000;
	private Activity activity;

	public static final String PREFS_NAME = "Preferences";
	public static final String PREFS_FIRST_TIMES = "first_times";
	public static final String PREFS_REMINDER_TIME = "reminder_time";
	public static final int PREFS_REMINDER_TIME_VALUE = 15;

	@Override
	public void onAttach(Activity activity) {
		this.activity = activity;
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle bundle) {
		final View view = inflater.inflate(R.layout.splash, container, false);
		createView((ViewGroup) view);
		setRetainInstance(true);
		return view;
	}
	
	@Override
	public void setUpListeners() {

	}
	
	@Override
	public void createView(ViewGroup view) {
		// TODO Auto-generated method stub
//		this.view = new SplashView(view);
		super.createView(view);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		final Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				showAnotherFragment();
			}
		}, DELAY);
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void showAnotherFragment() {
		FragmentBuilder.replaceFragment((FragmentActivity) activity, android.R.id.content, FragmentsEnum.FRAGMENT_ONE);



	}

	@Override
	public Activity getLocalActivity() {
		return activity;
	}

}
