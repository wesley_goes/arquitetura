package com.arquitetura.axm.axmarquitetura.view;

import android.view.ViewGroup;
import android.widget.TextView;

import com.arquitetura.axm.axmarquitetura.R;
import com.arquitetura.axm.axmarquitetura.view.interfaces.IFragmentTwoItemView;

/**
 * Created by Wesley Goes on 13/08/2015.
 */
public class FragmentTwoItemView implements IFragmentTwoItemView{

    private transient TextView txtName;
    private transient TextView txtAge;
     

    public FragmentTwoItemView(ViewGroup view){
        setTxtName(view);
        setTxtAge(view);
    }

    private void setTxtName(ViewGroup view) {
        if(view.findViewById(R.id.txt_name) instanceof  TextView){
            this.txtName = (TextView)view.findViewById(R.id.txt_name);
        }
    }

    private void setTxtAge(ViewGroup view) {
        if(view.findViewById(R.id.txt_age) instanceof  TextView){
            this.txtAge = (TextView)view.findViewById(R.id.txt_age);
        }
    }

    @Override
    public void setName(String name) {
        if(txtName != null){
            txtName.setText(name);
        }
    }

    @Override
    public void setAge(int age) {
        if(txtAge != null){
            txtAge.setText(age);
        }
    }



}
