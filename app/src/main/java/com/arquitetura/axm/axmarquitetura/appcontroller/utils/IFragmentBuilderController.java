package com.arquitetura.axm.axmarquitetura.appcontroller.utils;

import android.support.v4.app.FragmentActivity;

import com.arquitetura.axm.axmarquitetura.appcontroller.enums.ActionEnum;
import com.arquitetura.axm.axmarquitetura.appcontroller.enums.FragmentsEnum;


interface IFragmentBuilderController {
	void setActivity(FragmentActivity activity);
	void setContentId(final int contId);
//	void setAction(final ActionEnum action);
	void setAction(final ActionEnum action);
	void setFragmentId(final FragmentsEnum frament);
	void show();
	void removeLastFragment();
}
