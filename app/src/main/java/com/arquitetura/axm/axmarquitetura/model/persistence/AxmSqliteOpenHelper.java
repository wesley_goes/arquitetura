package com.arquitetura.axm.axmarquitetura.model.persistence;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.arquitetura.axm.axmarquitetura.model.domain.FuncionarioAxm;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

public class AxmSqliteOpenHelper extends OrmLiteSqliteOpenHelper{
	
	private static final String DATABASE_NAME = "inventariobd.sqlite";
	
	private static final int DATABASE_VERSION = 1;
	
	private static final String FOLDER = "/PhilcoInventario/";
	
	public AxmSqliteOpenHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase arg0, ConnectionSource arg1) {
//		WrapperLog.info("Criando a Tabela do Banco de Dados");
		
		try{
			TableUtils.createTable(connectionSource, FuncionarioAxm.class);

//			if(AppController.DEMO == true){
//				populateInitialData();
//			}

		}catch(SQLException e){
//			WrapperLog.info("Ocorreu um erro ao tentar criar a tabela do banco de dados."+ e.getMessage());
			throw new RuntimeException(e);
		}
	}
	
	private void populateInitialData() throws SQLException {
			
		 //inserindo dados na tabela
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase dataBase, ConnectionSource connectionSource, int oldVersion, int newVersion) {
//		WrapperLog.info("Atualizando Banco de dados. da vers?o[" + oldVersion + "] para a vers?o [" + newVersion + "]");
		
		try{
			TableUtils.dropTable(connectionSource, FuncionarioAxm.class, true);
			onCreate(dataBase, connectionSource);
		}catch(SQLException e){
//			WrapperLog.info("Ocorreu um erro ao tentar apagar a tabela do banco de dados" + e.getMessage());
			throw new RuntimeException(e);
		}
	}

}
