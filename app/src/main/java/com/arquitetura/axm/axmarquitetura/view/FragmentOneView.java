package com.arquitetura.axm.axmarquitetura.view;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.arquitetura.axm.axmarquitetura.R;
import com.arquitetura.axm.axmarquitetura.view.interfaces.IFragmentOneView;

/**
 * Created by Wesley Goes on 13/08/2015.
 */
public class FragmentOneView implements IFragmentOneView{

    private transient TextView txtClick;
    private transient Button btClick;

    public FragmentOneView(ViewGroup view){
        setBtClick(view);
    }

    private void setBtClick(ViewGroup view) {
        if(view.findViewById(R.id.bt_click) instanceof  Button){
            this.btClick = (Button)view.findViewById(R.id.bt_click);
        }
    }


    @Override
    public String getText() {
        return null;
    }

    @Override
    public void setText(String text) {

    }

    @Override
    public void setButtonOnclickListener(View.OnClickListener listener) {
        if (this.btClick != null){
            this.btClick.setOnClickListener(listener);
        }
    }
}
