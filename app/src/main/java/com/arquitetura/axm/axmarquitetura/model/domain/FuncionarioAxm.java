package com.arquitetura.axm.axmarquitetura.model.domain;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by wesleygoes on 15/08/15.
 */
@DatabaseTable(tableName = "axm")
public class FuncionarioAxm {

    @DatabaseField(generatedId = true, columnName = "employee_id")
    private int id;

    @DatabaseField(columnName = "employee_name")
    private String name;

    @DatabaseField(columnName = "employee_age")
    private int age;

    public int getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public int getAge() {

        return age;
    }

    public void setAge(int age) {

        this.age = age;
    }
}
