package com.arquitetura.axm.axmarquitetura.appcontroller.utils;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import com.arquitetura.axm.axmarquitetura.appcontroller.enums.ActionEnum;
import com.arquitetura.axm.axmarquitetura.appcontroller.enums.FragmentsEnum;
import com.arquitetura.axm.axmarquitetura.presenter.AbstractPresenter;


public class FragmentBuilder implements IFragmentBuilderController {
	private int contentId;
	private ActionEnum action;
	private FragmentsEnum fragmentId;
	private FragmentActivity activity;
	public int size;
	private static FragmentBuilder instance;
	private Bundle params;
	private final static int INIT_VALUE = 0;

	public static FragmentBuilder getInstance() {
		synchronized (FragmentBuilder.class) {
			if (instance == null) {
				instance = new FragmentBuilder();
			}
			return instance;
		}
	}

	public static void setInstance(final FragmentBuilder instance) {
		FragmentBuilder.instance = instance;
	}

	public void setSize(final int size) {
		this.size = size;
	}

	public int getSize() {
		return size;
	}

	public int getContentId() {
		return contentId;
	}

	public FragmentsEnum getFragmentId() {
		return fragmentId;
	}

	public void setActivity(final FragmentActivity activity) {
		this.activity = activity;
	}

	public int getId() {
		return contentId;
	}

	public ActionEnum getAction() {
		return action;
	}

	public Activity getActivity() {
		return activity;
	}

	@Override
	public void setContentId(final int contentId) {
		this.contentId = contentId;
	}

	@Override
	public void setAction(final ActionEnum action) {
		this.action = action;
	}

	@Override
	public void setFragmentId(final FragmentsEnum fragment) {
		this.fragmentId = fragment;
	}

	@Override
	public void show() {
		if (this.fragmentId != null) {
			final FragmentsEnum fragmentId = this.fragmentId;
			final Fragment fragment = SimpleFramentFactory.getInstance().create(fragmentId);
			if (this.params != null) {
				fragment.setArguments(this.params);
			}
			if (this.action == ActionEnum.ADD) {
				addFragment(fragment);
			} else if (this.action == ActionEnum.REPLACE) {
				replaceFragment(fragment);
			}
		}
	}

	public void addFragment(final Fragment fragment) {
		if (this.activity != null) {
			final int size = FragmentBuilder.getInstance().getSize() + 1;
			FragmentBuilder.getInstance().setSize(size);
			final String tag = String.valueOf(FragmentBuilder.getInstance().getSize());
			this.activity.getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).add(this.contentId, fragment, tag).commit();
		}

	}

	public void replaceFragment(final Fragment fragment) {
		final int size = FragmentBuilder.getInstance().getSize();
		if (this.activity != null && fragment != null && size > 0) {
			final String tag = String.valueOf(FragmentBuilder.getInstance().getSize());
			this.activity.getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).replace(this.contentId, fragment, tag).commitAllowingStateLoss();
		}
	}

	public void removeFragment(final Fragment fragment) {
		if (this.activity != null) {
			final int size = FragmentBuilder.getInstance().getSize() - 1;
			FragmentBuilder.getInstance().setSize(size);
			this.activity.getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE).remove(fragment).commit();
		}
	}

	public void addFragment(final Fragment fragment, final boolean backStack) {
		if (this.activity != null) {
			final int size = FragmentBuilder.getInstance().getSize() + 1;
			FragmentBuilder.getInstance().setSize(size);
			final String tag = String.valueOf(FragmentBuilder.getInstance().getSize());
			this.activity.getSupportFragmentManager().beginTransaction().add(this.contentId, fragment, tag).addToBackStack(tag).commit();
		}
	}

	@Override
	public void removeLastFragment() {
		if (this.activity != null) {
			final int lastItem = FragmentBuilder.getInstance().getSize();
			final String tag = String.valueOf(lastItem);
			final Fragment fragment = this.activity.getSupportFragmentManager().findFragmentByTag(tag);
			if (fragment != null) {
				removeFragment(fragment);
			}
		}
	}

	public AbstractPresenter getFragment(final FragmentActivity activity, final FragmentsEnum frag) {
		AbstractPresenter fragment = null;
		if (activity != null) {
//			Log.info("Fragment:" + activity.getSupportFragmentManager().getFragments());
			for (Fragment fra : activity.getSupportFragmentManager().getFragments()) {
				if (fra.getClass() == frag.getClassFrag()) {
					fragment = (AbstractPresenter) fra;
					break;
				}
			}
		}
//		Log.info("Fragment:" + fragment);
		return fragment;
	}

	public static void addFragment(final FragmentActivity activity, final int contentId, final FragmentsEnum fragment) {
		final FragmentBuilder framentBuilder = new FragmentBuilder();
		framentBuilder.setActivity(activity);
		framentBuilder.setContentId(contentId);
		framentBuilder.setAction(ActionEnum.ADD);
		framentBuilder.setFragmentId(fragment);
		framentBuilder.show();
	}

	public static void addFragment(final FragmentActivity activity, final int contentId, final FragmentsEnum fragment, final Bundle params) {
		final FragmentBuilder framentBuilder = new FragmentBuilder();
		framentBuilder.setActivity(activity);
		framentBuilder.setContentId(contentId);
		framentBuilder.setParams(params);
		framentBuilder.setAction(ActionEnum.ADD);
		framentBuilder.setFragmentId(fragment);
		framentBuilder.show();
	}

	public static void replaceFragment(final FragmentActivity activity, final int contentId, final FragmentsEnum fragment) {
		final FragmentBuilder framentBuilder = new FragmentBuilder();
		framentBuilder.setActivity(activity);
		framentBuilder.setContentId(contentId);
		framentBuilder.setAction(ActionEnum.REPLACE);
		framentBuilder.setFragmentId(fragment);
		framentBuilder.show();
	}

	public static void replaceFragment(final FragmentActivity activity, final int contentId, final FragmentsEnum fragment, final Bundle params) {
		final FragmentBuilder framentBuilder = new FragmentBuilder();
		framentBuilder.setActivity(activity);
		framentBuilder.setContentId(contentId);
		framentBuilder.setParams(params);
		framentBuilder.setAction(ActionEnum.REPLACE);
		framentBuilder.setFragmentId(fragment);
		framentBuilder.show();
	}

	public static void removeLastFragment(final FragmentActivity activity) {
		final FragmentBuilder framentBuilder = new FragmentBuilder();
		framentBuilder.setActivity(activity);
		framentBuilder.setAction(ActionEnum.REMOVE);
		framentBuilder.removeLastFragment();
	}

	public static void onBackPressed(final FragmentActivity activity) {
		if (FragmentBuilder.getInstance().getSize() > INIT_VALUE) {
			FragmentBuilder.removeLastFragment(activity);
		}

	}

	public Bundle getParams() {
		return params;
	}

	public void setParams(final Bundle params) {
		this.params = params;
	}

}
