package com.arquitetura.axm.axmarquitetura.presenter.events;

import android.view.View;
import android.view.View.OnClickListener;

import com.arquitetura.axm.axmarquitetura.presenter.interfaces.IFragmentOnePresenter;
import com.arquitetura.axm.axmarquitetura.presenter.interfaces.IFragmentTwoPresenter;


public class FragmentActionListener {
	
	public static class ShowFragmentOneOnClickListener implements OnClickListener{
		private final transient IFragmentOnePresenter presenter;
		public ShowFragmentOneOnClickListener(final IFragmentOnePresenter presenter) {
			this.presenter = presenter;
		}
		
		@Override
		public void onClick(final View view) {
			presenter.setText();
		}
	}


	public static class ShowFragmentTwoOnClickListener implements OnClickListener{
		private final transient IFragmentTwoPresenter presenter;
		public ShowFragmentTwoOnClickListener(final IFragmentTwoPresenter presenter) {
			this.presenter = presenter;
		}
		
		@Override
		public void onClick(final View view) {
			presenter.back();
		}
	}
}
