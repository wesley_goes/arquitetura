package com.arquitetura.axm.axmarquitetura.presenter.interfaces;

import android.support.v4.app.FragmentActivity;

import com.arquitetura.axm.axmarquitetura.view.interfaces.IFragmentTwoView;

public interface IFragmentTwoPresenter {
	FragmentActivity getLocalActivity();
	IFragmentTwoView getLocalView();
	void back();

}  