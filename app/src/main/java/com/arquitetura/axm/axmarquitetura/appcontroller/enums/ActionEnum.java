package com.arquitetura.axm.axmarquitetura.appcontroller.enums;

public enum ActionEnum {
	ADD(1),
	REPLACE(2),
	REMOVE(3);

	private int action;

	/**
	 * Constructor
	 */
	private ActionEnum(final int action) {
		this.action = action;
	}

	public int getAction() {
		return action;
	}
}
