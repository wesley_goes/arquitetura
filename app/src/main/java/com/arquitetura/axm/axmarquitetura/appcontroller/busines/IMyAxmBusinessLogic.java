package com.arquitetura.axm.axmarquitetura.appcontroller.busines;

import com.arquitetura.axm.axmarquitetura.model.domain.FuncionarioAxm;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by wesleygoes on 17/08/15.
 */
public interface IMyAxmBusinessLogic {

    <T>T searchById(Class<T> clazz, Long id) throws SQLException;
    <T>T searchByItem(String item, String local) throws SQLException;
    <T>T searchByItemNew(String item) throws SQLException;

    void insertAxmArquitetura(FuncionarioAxm funcionarioAxm) throws SQLException, ObjectAlreadyExistException;
    void updateAxmArquitetura(FuncionarioAxm funcionarioAxm) throws SQLException;
    void deleteAxmArquitetura(FuncionarioAxm funcionarioAxm) throws SQLException;
    void deleteAll() throws SQLException;
    List<FuncionarioAxm> getEmployee() throws SQLException;
    List<FuncionarioAxm> getEmployeeReport(final boolean state) throws SQLException;
    List<FuncionarioAxm> getEmployeeVerify() throws SQLException;

}
