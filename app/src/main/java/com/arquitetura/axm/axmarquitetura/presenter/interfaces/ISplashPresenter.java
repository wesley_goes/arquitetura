package com.arquitetura.axm.axmarquitetura.presenter.interfaces;

import android.app.Activity;

public interface ISplashPresenter {
	void showAnotherFragment(); 
	Activity getLocalActivity();
}
